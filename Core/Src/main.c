/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "can.h"
#include "i2c.h"
#include "i2s.h"
#include "spi.h"
#include "usart.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DATA_SIZE 8
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
CAN_TxHeaderTypeDef TxHeader;
CAN_RxHeaderTypeDef RxHeader;


uint32_t TxMailBox;

uint8_t TxData[8];
uint8_t RxData[8];

uint8_t LightRightOn = 0;
uint8_t LightLftOn = 0;
uint8_t firstTime = 1;

void InitPins(void)
{
	HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(L3_GPIO_Port, L3_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(L4_GPIO_Port, L4_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_SET);
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData);
	 /* TxHeader.DLC = 8;
	  TxHeader.IDE = CAN_ID_STD;
	  TxHeader.StdId = 0x111;
	  TxHeader.RTR = CAN_RTR_DATA;
	  TxHeader.TransmitGlobalTime = DISABLE;
//	  TxData = RxData;
	  int len = sizeof(RxData)/sizeof(uint8_t);
	  for (int i = 0 ;i < 8; i++)
	  {
		  TxData[i] = RxData[i];
	  }
	  HAL_CAN_AddTxMessage(&hcan1, &TxHeader,TxData, &TxMailBox);*/


	//HAL_UART_Transmit(&huart2, RxData, sizeof(RxData),HAL_MAX_DELAY);
	if(RxHeader.StdId == 0x123 && RxHeader.DLC == 1)
	{
		switch (RxData[0])
		{
			case LIGHT_BACK_ML_ON:
				HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin, GPIO_PIN_RESET);
				break;
			case LIGHT_BACK_ML_OFF:
				HAL_GPIO_WritePin(L1_GPIO_Port, L1_Pin, GPIO_PIN_SET);
				break;
			case LIGHT_FOG_ON:
				HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin, GPIO_PIN_RESET);
				break;
			case LIGHT_FOG_OFF:
				HAL_GPIO_WritePin(L2_GPIO_Port, L2_Pin, GPIO_PIN_SET);
				break;
			case LIGHT_BREAK_ON:
				HAL_GPIO_WritePin(L3_GPIO_Port, L3_Pin, GPIO_PIN_RESET);
				break;
			case LIGHT_BREAK_OFF:
				HAL_GPIO_WritePin(L3_GPIO_Port,L3_Pin, GPIO_PIN_SET);
				break;
			case LIGHT_REVERSE_ON:
				HAL_GPIO_WritePin(L4_GPIO_Port,L4_Pin,GPIO_PIN_RESET);
				break;
			case LIGHT_REVERSE_OFF:
				HAL_GPIO_WritePin(L4_GPIO_Port, L4_Pin,GPIO_PIN_SET);
				break;
			case LIGHT_LEFT_ON:
				HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin, GPIO_PIN_SET);
				LightLftOn = 1;
				LightRightOn = 0;
				firstTime =1;
				break;
			case LIGHT_LEFT_OFF:
				HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);
				LightLftOn = 0;
				break;
			case LIGHT_RIGHT_ON:
				HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin, GPIO_PIN_SET);
				LightRightOn = 1;
				LightLftOn = 0;
				firstTime = 1;
				break;
			case LIGHT_RIGHT_OFF:
				HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_SET);
				LightRightOn = 0;
				firstTime = 1;
				break;
			case LIGHTS_BOTH_ON:
				HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);
				LightRightOn = 1;
				LightLftOn = 1;
				firstTime = 1;
				break;
			case LIGHTS_BOTH_OFF:
				HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);
				LightRightOn = 0;
				LightLftOn = 0;
				firstTime = 1;
				break;


		}


	}
	//HAL_GPIO_WritePin(L6_GPIO_Port, L6_Pin, GPIO_PIN_SET);
	//HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	//count++;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2S3_Init();
  MX_SPI1_Init();
  MX_USB_HOST_Init();
  MX_CAN1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  CAN1Configuration();
  InitPins();


  /*TxHeader.DLC = 1;
  TxHeader.IDE = CAN_ID_STD;
  TxHeader.StdId = 0x111;
  TxHeader.RTR = CAN_RTR_DATA;
  TxHeader.TransmitGlobalTime = DISABLE;
  TxData[0] = 0xf3;
  HAL_CAN_AddTxMessage(&hcan1, &TxHeader, &TxData[0], &TxMailBox);*/

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	  HAL_GPIO_TogglePin(LD6_GPIO_Port, LD6_Pin);
	  HAL_Delay(650);
	  if(LightLftOn == 1 && LightRightOn == 1)
	  {
		  	if(firstTime == 1)
		  	{
		  		HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin,GPIO_PIN_SET);
		  		HAL_GPIO_WritePin(L5_GPIO_Port, L5_Pin, GPIO_PIN_SET);
		  		firstTime = 0;
		  	}

			HAL_GPIO_TogglePin(L5_GPIO_Port, L5_Pin);
			HAL_GPIO_TogglePin(L6_GPIO_Port, L6_Pin);
			HAL_Delay(500);

	  }
	  else if(LightLftOn == 1&& LightRightOn == 0)
	  {
		  HAL_GPIO_TogglePin(L5_GPIO_Port, L5_Pin);
		  HAL_Delay(500);
	  }
	  else if(LightLftOn == 0 && LightRightOn == 1)
	  {
		  HAL_GPIO_TogglePin(L6_GPIO_Port, L6_Pin);
		  HAL_Delay(500);

	  }
//

//	  if(LightLftOn == 1 && LightRightOn == 0)
//	  {
//
//		  HAL_GPIO_TogglePin(L5_GPIO_Port, L5_Pin);
//		  HAL_Delay(500);
//	  }
//	  else if(LightRightOn == 1 && LightLftOn ==0)
//	  {
//
//		  HAL_GPIO_TogglePin(L6_GPIO_Port, L6_Pin);
//		  HAL_Delay(500);
//	  }
//	  else if (LightRightOn == 1 && LightLftOn ==1)
//	  {
//		  HAL_GPIO_TogglePin(L5_GPIO_Port, L5_Pin);
//		  HAL_GPIO_TogglePin(L6_GPIO_Port, L6_Pin);
//		  HAL_Delay(500);
//	  }
	  /*count++;
	  TxData[0] += 1;
	  HAL_CAN_AddTxMessage(&hcan1, &TxHeader, &TxData[0], &TxMailBox);*/
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */


  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
