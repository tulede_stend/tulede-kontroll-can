#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"

//GENERAL
#define TRUE  1
#define FALSE 0

//STRUCTS

// Vehicle body controller signals
struct main_body_signals {
	uint8_t		door_signal;
	uint8_t		door_feedback_signal;
};


struct timers {

	uint32_t ESP_timer_door;

	//for debuging purpose
	uint32_t can1_receive;
	uint32_t can2_receive;
	uint32_t car_control_thread;
};

void setStartingParameters(void);

void handle_UI_Switches(void);
void handle_UI_LEDs(void);

void send_door_command(void);
