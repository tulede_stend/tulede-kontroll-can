#include "stdint.h"

#define CONTROL_THREAD 		1
#define CAN1_RECEIVE		2

#define CAR_CONTROL_LED_PERIOD 	1000
#define CAN1_RECEIVE_LED_PERIOD 50

void blinkLed(uint8_t led);

