################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (12.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../newMain/inc/newMain.c 

OBJS += \
./newMain/inc/newMain.o 

C_DEPS += \
./newMain/inc/newMain.d 


# Each subdirectory must supply rules for building sources it contributes
newMain/inc/%.o newMain/inc/%.su newMain/inc/%.cyclo: ../newMain/inc/%.c newMain/inc/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F407xx -c -I../USB_HOST/App -I../USB_HOST/Target -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Middlewares/ST/STM32_USB_Host_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-newMain-2f-inc

clean-newMain-2f-inc:
	-$(RM) ./newMain/inc/newMain.cyclo ./newMain/inc/newMain.d ./newMain/inc/newMain.o ./newMain/inc/newMain.su

.PHONY: clean-newMain-2f-inc

